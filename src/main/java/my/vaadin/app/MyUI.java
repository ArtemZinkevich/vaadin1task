package my.vaadin.app;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import my.vaadin.app.components.CategoriesLayout;
import my.vaadin.app.components.CategoryView;
import my.vaadin.app.components.HotelLayout;
import my.vaadin.app.components.HotelView;
import my.vaadin.app.service.CategoryService;
import my.vaadin.app.service.HotelService;

@Theme("mytheme")
@SpringUI
public class MyUI extends UI {
	@Autowired
	private HotelLayout hotels;
	@Autowired
	private CategoriesLayout categories;
	@Autowired
	private HotelView hotelView;
	@Autowired
	private CategoryView categoryView;
	private MenuBar menuBar;
	private Navigator navigator;
	private AtomicBoolean isInitialized = new AtomicBoolean(false);

	@WebListener
	public static class MyContextLoaderListener extends ContextLoaderListener {
	}

	@Configuration
	@EnableVaadin
	public static class MyConfiguration {
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		if (!isInitialized.get()) {
			initializeLayouts();
		}
		final VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.addComponent(getMenuBar());
		Panel mainPanel = new Panel();
		mainLayout.addComponent(mainPanel);
		mainPanel.setSizeFull();
		setSizeFull();
		mainPanel.setId("mainPanel");
		mainLayout.setId("mainLayout");
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(mainPanel, 100);
		mainLayout.setMargin(false);
		navigator = new Navigator(this, mainPanel);
		navigator.addView(HotelView.VIEW_NAME, hotelView);
		navigator.addView(CategoryView.VIEW_NAME, categoryView);
		navigator.navigateTo(HotelView.VIEW_NAME);
		setContent(mainLayout);
	}
	private void initializeLayouts() {
		hotels.initHotelLayout();
		categories.initCategoryLayout();
		hotelView.setLayout(hotels);
		categoryView.setLayout(categories);
		isInitialized.set(true);
	}
	private MenuBar getMenuBar() {
		if (menuBar != null) {
			return menuBar;
		}
		// create menu bar
		menuBar = new MenuBar();

		// create menu items
		menuBar.addItem("Hotel", command -> navigator.navigateTo(HotelView.VIEW_NAME));
		menuBar.addItem("", null).setEnabled(false);
		menuBar.addItem("Categories", command -> navigator.navigateTo(CategoryView.VIEW_NAME));

		return menuBar;
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	public static class MyUIServlet extends SpringVaadinServlet {
	}
}

package my.vaadin.app.components;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;

import my.vaadin.app.entity.Category;
import my.vaadin.app.service.CategoryService;

@SpringComponent
@UIScope
public class CategoriesLayout extends CustomComponent {
	@Autowired
	private CategoryService service;
	private Grid<Category> grid = new Grid<>(Category.class);
	@Autowired
	private CategoryForm form;
	@Autowired
	private HotelLayout connected;

	public CategoriesLayout() {
	}

	public void initCategoryLayout() {
		form.initCategoryForm(this);
		form.setVisible(false);
		Button addCategoryBtn = new Button("Add");
		addCategoryBtn.addClickListener(e -> {
			grid.asMultiSelect().clear();
			form.setCategory(new Category());
			form.setVisible(true);
		});
		Button editBtn = new Button("edit");
		editBtn.addClickListener(e -> {
			Category category = grid.getSelectedItems().iterator().next();
			grid.asMultiSelect().clear();
			form.setCategory(category);
			editBtn.setEnabled(false);
			form.setVisible(true);
		});
		editBtn.setEnabled(false);
		Button deleteBtn = new Button("delete");
		deleteBtn.addClickListener(e -> {
			Set<Category> categories = grid.getSelectedItems();
			connected.updateList();
			grid.asMultiSelect().clear();
			categories.forEach(element -> {
				service.remove(element);
			});
			updateList();
			connected.updateList();
			editBtn.setEnabled(false);
			deleteBtn.setEnabled(false);
		});
		deleteBtn.setEnabled(false);
		HorizontalLayout menu = new HorizontalLayout(addCategoryBtn, deleteBtn, editBtn);
		final VerticalLayout layout = new VerticalLayout();
		grid.setColumns("name");
		grid.setItems(service.findAll());
		MultiSelectionModel<Category> selectionModel = (MultiSelectionModel<Category>) grid
				.setSelectionMode(SelectionMode.MULTI);
		selectionModel.addMultiSelectionListener(event -> {
			if (!event.getAllSelectedItems().isEmpty()) {
				deleteBtn.setEnabled(true);
				editBtn.setEnabled(event.getAllSelectedItems().size() == 1);
			} else {
				editBtn.setEnabled(false);
				deleteBtn.setEnabled(false);
			}

		});
		grid.setSizeUndefined();
		layout.addComponents(menu, grid, form);
		setCompositionRoot(layout);
	}

	public void updateList() {
		List<Category> categories = service.findAll();
		grid.setItems(categories);
		connected.updateList();
	}

	public void setConnected(HotelLayout connected) {
		this.connected = connected;
	}

}

package my.vaadin.app.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.entity.Category;
import my.vaadin.app.service.CategoryService;

@SpringComponent
@UIScope
public class CategoryForm extends FormLayout {
	@Autowired
	private CategoryService service; 
	private TextField name = new TextField("Name");
	private Button save = new Button("Save");
	private Category category;
	private CategoriesLayout layout;
	private Binder<Category> binder;

	public CategoryForm() {
	}

	public void initCategoryForm(CategoriesLayout layout) {
		binder = new Binder<>(Category.class);
		this.layout = layout;
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save);
		addComponents(name, buttons);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);
		binder.bindInstanceFields(this);
		save.addClickListener(e -> this.save());
	}

	public void setCategory(Category category) {
		this.category = category;
		binder.setBean(category);
		setVisible(true);
		name.selectAll();
	}

	private void save() {
		category.setName(name.getValue());
		service.save(category);
		layout.updateList();
		setVisible(false);

	}

}

package my.vaadin.app.components;

import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
@UIScope
public class CategoryView extends VerticalLayout implements View {

	private CategoriesLayout layout;
	public static String VIEW_NAME = "categories";

	public void setLayout(CategoriesLayout layout) {
		this.layout = layout;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		addComponent(layout);
	}

}
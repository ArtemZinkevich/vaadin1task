package my.vaadin.app.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.MyUI;
import my.vaadin.app.converter.DateConverter;
import my.vaadin.app.converter.StringToCategoryConverter;
import my.vaadin.app.entity.Category;
import my.vaadin.app.entity.Hotel;
import my.vaadin.app.entity.Payment;
import my.vaadin.app.service.CategoryService;
import my.vaadin.app.service.HotelService;

@SpringComponent
@UIScope
public class HotelForm extends FormLayout {
	@Autowired
	private HotelService service;
	@Autowired
	private CategoryService categoryService;
	private TextField name = new TextField("Name");
	private TextField address = new TextField("Address");
	private TextField rating = new TextField("Rating");
	private TextArea description = new TextArea("Description");
	private DateField operatesFrom = new DateField("Operates from");
	private NativeSelect<Category> category = new NativeSelect<>("Hotel category");
	private TextField url = new TextField("URL");
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	private PaymentField payment = new PaymentField();
	{
		payment.addValueChangeListener(event -> {
			Payment oldValue = event.getOldValue();
			Payment currentValue = event.getValue();
			 if(oldValue!=null&&currentValue!=null) {
				Integer previous = oldValue.getGuaranteeFee();
				if (previous == null) {
					Notification
							.show("Old payment type for this field was Cash. You switch it to credit card and guaranty fee value : "
									+ currentValue.getGuaranteeFee(), Notification.Type.HUMANIZED_MESSAGE);
				} else {
					Notification.show(
							"Old guaranty fee for this hotel was: " + oldValue.getGuaranteeFee()
									+ ". You enter value : " + currentValue.getGuaranteeFee(),
							Notification.Type.HUMANIZED_MESSAGE);
				}
			}
		});
	}

	private Button save = new Button("Save", event -> {
		save();
	});
	private Button cancel = new Button("Cancel", e -> {
		binder.removeBean();
		this.setVisible(false);
	});
	private Hotel hotel;
	private HotelLayout layout;
	private HorizontalLayout buttons = new HorizontalLayout(save, cancel);

	public HotelForm() {
	}

	public void initForm(HotelLayout myUI) {
		this.layout = myUI;
		setSizeUndefined();
		addComponents(name, address, payment, rating, description, operatesFrom, category, url, buttons);
		category.setItemCaptionGenerator(Category::getName);
		category.setItems(categoryService.findAll());
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);
		addFieldToolTips();
		bindFields();
		setMargin(false);

	}

	// Binding fields to binder.
	private void bindFields() {
		binder.forField(name).asRequired("Name must be not empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).asRequired("Address must be not empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(rating).withNullRepresentation("").asRequired("obligatory field")
				.withConverter(new StringToIntegerConverter(0, "must be a digit!"))
				.withValidator(value -> value >= 0, "must be more or equal to 0")
				.withValidator(value -> value <= 5, "must be less or equal to 5")
				.bind(Hotel::getRating, Hotel::setRating);
		binder.forField(operatesFrom).asRequired("obligatory field").withConverter(new DateConverter())
				.withValidator(days -> days >= 0, "Must be less than today.")
				.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
		binder.forField(category).asRequired("must be chosen").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).asRequired("url must be not empty").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(payment).bind(Hotel::getPayment, Hotel::setPayment);
	}

	private void addFieldToolTips() {
		name.setDescription("Hotel name");
		address.setDescription("Hotel address");
		rating.setDescription("Hotel rating");
		operatesFrom.setDescription("Hotel operating date");
		description.setDescription("Hotel description");
		category.setDescription("Hotel category");
		url.setDescription("Hotel url");
		save.setDescription("Save hotel");
		cancel.setDescription("Cancel and close form");
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
		binder.readBean(hotel);
		setVisible(true);
		name.selectAll();
	}

	private void delete() {
		service.remove(hotel);
		layout.updateList();
		setVisible(false);
	}

	private void save() {
		boolean result = binder.writeBeanIfValid(hotel);
		if (result && payment.isValid()) {
			service.save(hotel);
			binder.removeBean();
			layout.updateList();
			setVisible(false);
		} else {
			Notification.show("Please, fill in the form correctly.", Notification.Type.WARNING_MESSAGE);
		}
	}

	public void updateCategories() {
		category.setItems(categoryService.findAll());
	}
}

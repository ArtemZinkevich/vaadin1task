package my.vaadin.app.components;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.entity.Hotel;
import my.vaadin.app.service.HotelService;

@SpringComponent
@UIScope
public class HotelLayout extends CustomComponent {
	@Autowired
	private HotelService service;
	@Autowired
	private HotelForm form;
	@Autowired
	private HotelPopUpForm popUpForm;
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private TextField nameFilter = new TextField();
	private TextField addressFilter = new TextField();
	private HorizontalLayout toolbar;
	private Button addHotelBtn;
	private Button editHotelBtn;
	private Button deleteHotelBtn;
	private Button bulkUpdateBtn;
	private VerticalLayout popupContent;
	private PopupView popup;
	private MultiSelectionModel<Hotel> selectionModel = (MultiSelectionModel<Hotel>) grid
			.setSelectionMode(SelectionMode.MULTI);
	{
		selectionModel.addMultiSelectionListener(event -> {
			form.setVisible(false);
			adjustToolbarButtonAccessibility(event.getAllSelectedItems().size());
		});
	}
	private Button updateBulk = new Button("Update", event -> {
		Object currentValue = popUpForm.receiveCurrentFieldValue();
		Set<Hotel> selected = selectionModel.getSelectedItems();
		boolean result = popUpForm.save(selected, currentValue);
		popup.setPopupVisible(false);
		popUpForm.clear();
		if (result) {
			Notification.show("Updated successfully.", Notification.Type.HUMANIZED_MESSAGE);
		} else {
			Notification.show("Wrong Data.Update denied.", Notification.Type.ERROR_MESSAGE);
		}
	});
	private Button cancelBulk = new Button("Cancel", event -> {
		popup.setPopupVisible(false);
		popUpForm.clear();
	});

	public HotelLayout() {
	}

	public void initHotelLayout() {
		popUpForm.setLayout(this);
		form.initForm(this);
		VerticalLayout hotels = new VerticalLayout();
		nameFilter.setPlaceholder("filter by name...");
		addressFilter.setPlaceholder("filter by address...");
		nameFilter.addValueChangeListener(e -> updateList());
		nameFilter.setValueChangeMode(ValueChangeMode.LAZY);
		addressFilter.addValueChangeListener(e -> updateList());
		addressFilter.setValueChangeMode(ValueChangeMode.LAZY);
		Button clearFilterTextBtn = new Button(VaadinIcons.CLOSE);
		clearFilterTextBtn.setDescription("Clear both filters");
		clearFilterTextBtn.addClickListener(e -> {
			nameFilter.clear();
			addressFilter.clear();
		});
		CssLayout filtering = new CssLayout();
		filtering.addComponents(nameFilter, addressFilter, clearFilterTextBtn);
		filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		toolbar = recieveToolBar();
		initGrid();
		HorizontalLayout main = new HorizontalLayout(grid, form);
		main.setSizeFull();
		grid.setSizeFull();
		main.setExpandRatio(grid, 1);
		form.setVisible(false);
		// fetch list of Customers from service and assign it to Grid
		updateList();
		initPopUp();
		HorizontalLayout popUpContentToolBar = new HorizontalLayout();
		popUpContentToolBar.addComponents(updateBulk, cancelBulk);
		popupContent = new VerticalLayout();
		popupContent.setCaption("Bulk update");
		popupContent.addComponent(popUpForm);
		popupContent.addComponents(popUpContentToolBar);
		popup = new PopupView(null, popupContent);
		popup.setSizeFull();
		popup.setVisible(false);
		popup.setHideOnMouseOut(false);

		adjustToolbarButtonAccessibility(0);
		hotels.addComponents(filtering, popup, toolbar, main);
		setCompositionRoot(hotels);
	}

	public void updateList() {
		List<Hotel> hotels = service.findAll(nameFilter.getValue(), addressFilter.getValue());
		grid.setItems(hotels);
		form.updateCategories();
	}

	private void initPopUp() {
		popUpForm.initForm();
	}

	private void initGrid() {
		grid.setColumns("name", "address", "operatesFrom");
		Column<Hotel, String> categoryColumn = grid.addColumn(hotel -> {
			return (hotel.getCategory() == null) ? "Category not Defined" : hotel.getCategory().getName();
		});
		categoryColumn.setCaption("category");
		Column<Hotel, String> urlColumn = grid.addColumn(
				hotel -> "<a href='" + hotel.getUrl() + "' target='_blank'>" + hotel.getUrl() + "</a>",
				new HtmlRenderer());
		urlColumn.setCaption("url");
		grid.addColumn("description");
	}

	private HorizontalLayout recieveToolBar() {
		if (toolbar == null) {
			addHotelBtn = new Button("Add new hotel");
			addHotelBtn.addClickListener(e -> {
				grid.asMultiSelect().clear();
				form.setHotel(new Hotel());
			});
			editHotelBtn = new Button("Edit hotel");
			editHotelBtn.addClickListener(e -> {
				Set<Hotel> selected = grid.asMultiSelect().getSelectedItems();
				grid.asMultiSelect().clear();
				Hotel selectedHotel = selected.iterator().next();
				form.setHotel(selectedHotel);
			});
			deleteHotelBtn = new Button("Delete hotel(s)");
			deleteHotelBtn.addClickListener(e -> {
				Set<Hotel> selected = grid.asMultiSelect().getSelectedItems();
				grid.asMultiSelect().clear();
				service.remove(selected);
				updateList();
			});
			bulkUpdateBtn = new Button("Bulk update");
			bulkUpdateBtn.addClickListener(e -> {
				popup.setVisible(true);
				popup.setPopupVisible(true);
			});
			HorizontalLayout toolbar = new HorizontalLayout(addHotelBtn, editHotelBtn, deleteHotelBtn, bulkUpdateBtn);
			this.toolbar = toolbar;
		}
		return toolbar;
	}

	private void adjustToolbarButtonAccessibility(int selectedAmount) {
		boolean isEditable = selectedAmount == 1;
		boolean isDeleteableOrUpdateable = selectedAmount >= 1;
		editHotelBtn.setEnabled(isEditable);
		deleteHotelBtn.setEnabled(isDeleteableOrUpdateable);
		bulkUpdateBtn.setEnabled(isDeleteableOrUpdateable);
	}

}

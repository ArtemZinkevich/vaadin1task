package my.vaadin.app.components;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.AbstractDateField;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractLocalDateField;
import com.vaadin.ui.AbstractSingleSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.PopupView;

import my.vaadin.app.converter.DateConverter;
import my.vaadin.app.entity.Category;
import my.vaadin.app.entity.Hotel;
import my.vaadin.app.service.CategoryService;
import my.vaadin.app.service.HotelService;

@SpringComponent
@UIScope
public class HotelPopUpForm extends FormLayout implements PopupView.Content {
	@Autowired
	private HotelService service;
	@Autowired
	private CategoryService categoryService;
	public void setLayout(HotelLayout layout) {
		this.layout = layout;
	}

	private HotelLayout layout;
	private TextField name = new TextField("Name");
	private TextField address = new TextField("Address");
	private TextField rating = new TextField("Rating");
	private TextArea description = new TextArea("Description");
	private DateField operatesFrom = new DateField("Operates from");
	private NativeSelect<Category> category = new NativeSelect<>("Hotel category");
	private ComboBox<AbstractComponent> fieldsSelect = new ComboBox<>("Field select");
	private TextField url = new TextField("URL");
	private Hotel hotel;
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	private AbstractComponent current;
	private TextField textFieldMock = new TextField();

	public void initForm() {
		makeFieldsInvisible();
		textFieldMock.setEnabled(false);
		textFieldMock.setPlaceholder("To edit select any field from above");
		fieldsSelect.setPlaceholder("Please select field");
		fieldsSelect.setItemCaptionGenerator(AbstractComponent::getCaption);
		fieldsSelect.setCaption("Hotel Field");
		fieldsSelect.setEmptySelectionCaption("Choose one field to edit");
		fieldsSelect.setItems(name, address, rating, description, operatesFrom, category, url);
		fieldsSelect.addSelectionListener(event -> {
			makeFieldsInvisible();
			// Optional is a little bit senseless here. Just making fun of some
			// java 8 features.
			Optional<AbstractComponent> selected = event.getSelectedItem();
			current = selected.orElse(null);
			if (current != null) {
				textFieldMock.setVisible(false);
				current.setVisible(true);
			} else {
				textFieldMock.setVisible(true);
			}
		});
		setSizeUndefined();
		addComponents(fieldsSelect, name, address, rating, description, operatesFrom, category, url, textFieldMock);
		category.setItemCaptionGenerator(Category::getName);
		category.setItems(categoryService.findAll());
		addFieldToolTips();
		bindFields();
	}

	private void makeFieldsInvisible() {
		name.setVisible(false);
		address.setVisible(false);
		rating.setVisible(false);
		description.setVisible(false);
		operatesFrom.setVisible(false);
		category.setVisible(false);
		url.setVisible(false);
	}

	private void bindFields() {
		binder.forField(name).asRequired("Name must be not empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).asRequired("Address must be not empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(rating).withNullRepresentation("").asRequired("obligatory field")
				.withConverter(new StringToIntegerConverter(0, "must be a digit!"))
				.withValidator(value -> value >= 0, "must be more or equal to 0")
				.withValidator(value -> value <= 5, "must be less or equal to 5")
				.bind(Hotel::getRating, Hotel::setRating);
		binder.forField(operatesFrom).asRequired("obligatory field").withConverter(new DateConverter())
				.withValidator(days -> days >= 0, "Must be less than today.")
				.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
		binder.forField(category).asRequired("must be chosen").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).asRequired("url must be not empty").bind(Hotel::getUrl, Hotel::setUrl);
	}

	private void addFieldToolTips() {
		name.setDescription("Hotel name");
		address.setDescription("Hotel address");
		rating.setDescription("Hotel rating");
		operatesFrom.setDescription("Hotel operating date");
		description.setDescription("Hotel description");
		category.setDescription("Hotel category");
		url.setDescription("Hotel url");
	}

	public boolean save(Set<Hotel> hotels, Object replacement) {
		boolean isValid = false;
		for (Hotel hotel : hotels) {
			binder.readBean(hotel);
			replaceCurrentField(replacement);
			isValid = binder.writeBeanIfValid(hotel);
			if (!isValid) {
				return isValid;
			}
			service.save(hotel);
			binder.removeBean();
		}
		if(isValid){
			layout.updateList();
		}
		return isValid;
	}

	public Object receiveCurrentFieldValue() {
		Object res = null;
		HasValue current = (HasValue) this.current;
		res = current.getValue();
		if (res instanceof AbstractSingleSelect) {
			AbstractSingleSelect select = (AbstractSingleSelect) res;
			res = select.getValue();
		}
		return res;
	}

	private void replaceCurrentField(Object replacement) {
		AbstractField field = null;
		AbstractSingleSelect select = null;
		AbstractLocalDateField dateField = null;
		if (current instanceof AbstractLocalDateField) {
			dateField = (AbstractLocalDateField) current;
		} else if (current instanceof AbstractSingleSelect) {
			select = (AbstractSingleSelect) current;
		} else {
			field = (AbstractField) current;
		}
		if (field != null) {
			field.setValue(((String) replacement));
		} else if (select != null) {
			select.setValue(((Category) replacement));
		} else if (dateField != null) {
			dateField.setValue(((LocalDate) replacement));
		}
	}
public void clear(){
	current=null;
	binder.removeBean();
	fieldsSelect.setSelectedItem(null);
	textFieldMock.setVisible(true);
	initForm();
}
	@Override
	public String getMinimizedValueAsHTML() {
		return null;
	}

	@Override
	public com.vaadin.ui.Component getPopupComponent() {
		return this;
	}
}

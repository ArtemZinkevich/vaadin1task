package my.vaadin.app.components;

import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
@UIScope
public class HotelView extends VerticalLayout implements View {

	private  HotelLayout layout;

	public void setLayout(HotelLayout layout) {
		this.layout = layout;
	}

	public static String VIEW_NAME = "hotels";

	@Override
	public void enter(ViewChangeEvent event) {
		layout.initHotelLayout();
		addComponent(layout);
	}

}
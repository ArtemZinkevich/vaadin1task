package my.vaadin.app.components;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import my.vaadin.app.entity.Payment;

public class PaymentField extends CustomField<Payment> {
	private static final String CREDIT_CARD = "Credit Card";
	private static final String CASH = "Cash";
	private RadioButtonGroup<String> paymentType = new RadioButtonGroup<>("");
	private TextField paymentValue = new TextField("Guaranty fee", "Guaranty Deposit");
	private Label cashTypeMessage = new Label("Payment will be made directly in the hotel");
	private Binder<Payment> binder = new Binder<>(Payment.class);
	private Payment payment;
	{
		paymentValue.addValueChangeListener(event -> {
			Payment old = new Payment(payment);
			if (payment != null) {
				binder.writeBeanIfValid(payment);
			}
			if (binder.isValid()) {
				ValueChangeEvent<Payment> fireEvent = createValueChange(old, true);
				fireEvent(fireEvent);
			}
		});
	}

	private void initRadioButton() {
		paymentType.setItems(CREDIT_CARD, CASH);
		paymentType.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		paymentType.addValueChangeListener(listener -> {
			String current = listener.getValue();
			if (current.equals(CREDIT_CARD)) {
				if (payment == null) {
					setValue(new Payment());
				}
			} else {
				setValue(null);
			}
			updateFieldVisibility();
		});
	}

	private void bindTextField() {
		binder.forField(paymentValue).withNullRepresentation("")
				.withConverter(new StringToIntegerConverter("Must be a digit!"))
				.withValidator(value -> value >= 0, "must be more or equal to 0")
				.withValidator(value -> value <= 100, "must be less or equal to 100")
				.bind(Payment::getGuaranteeFee, Payment::setGuaranteeFee);
	}

	private void updateFieldVisibility() {
		if (payment == null) {
			paymentType.setSelectedItem(CASH);
			cashTypeMessage.setVisible(true);
			paymentValue.setVisible(false);
		} else {
			paymentType.setSelectedItem(CREDIT_CARD);
			cashTypeMessage.setVisible(false);
			paymentValue.setVisible(true);
		}
	}

	@Override
	public Payment getValue() {
		return payment;
	}

	@Override
	protected Component initContent() {
		VerticalLayout res = new VerticalLayout();
		res.addComponents(paymentType, paymentValue, cashTypeMessage);
		bindTextField();
		initRadioButton();
		updateFieldVisibility();
		res.setMargin(false);
		setCaption("Payment type");
		return res;
	}

	@Override
	protected void doSetValue(Payment value) {
		if (value == null) {
			payment = value;
		} else {
			this.payment = new Payment(value);
		}
		binder.readBean(payment);
		updateFieldVisibility();
	}
	public boolean isValid(){
		return payment==null||binder.isValid();
	}

}

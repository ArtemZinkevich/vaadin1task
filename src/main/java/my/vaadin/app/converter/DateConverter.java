package my.vaadin.app.converter;

import java.time.Duration;
import java.time.LocalDate;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class DateConverter implements Converter<LocalDate, Long> {

	@Override
	public Result<Long> convertToModel(LocalDate value, ValueContext context) {
		Result<Long> result;
		if (value == null) {
			result = Result.ok(null);
		} 
			long days = Duration.between(value.atTime(0, 0), LocalDate.now().atTime(0, 0)).toDays();
			result= Result.ok(days);
			return result;
		
		
	}

	@Override
	public LocalDate convertToPresentation(Long value, ValueContext context) {
		LocalDate result;
		if(value==null){
			result=LocalDate.now();
		} else{
			result = LocalDate.now().minusDays(value);
		}
		return result;
	}

}

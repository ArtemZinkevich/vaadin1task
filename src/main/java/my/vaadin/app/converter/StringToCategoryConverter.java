package my.vaadin.app.converter;

import java.time.Duration;
import java.time.LocalDate;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import my.vaadin.app.entity.Category;
import my.vaadin.app.service.CategoryService;

public class StringToCategoryConverter implements Converter<String,Category> {

	@Override
	public Result<Category> convertToModel(String value, ValueContext context) {
		Result<Category> result;
		if (value == null) {
			result = Result.ok(null);
		} else{
			result = Result.ok(new Category(value));
		}
		
			return result;
	}

	@Override
	public String convertToPresentation(Category value, ValueContext context) {
		String result;
		if(value==null){
			result = "Category is Undefined";
		}else{
			result = value.getName();
		}
		return result;
	}

}

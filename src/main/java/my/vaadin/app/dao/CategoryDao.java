package my.vaadin.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import my.vaadin.app.entity.AbstractEntity;
import my.vaadin.app.entity.Category;
import my.vaadin.app.entity.Hotel;

@Repository
public class CategoryDao implements IDAO {
	@PersistenceContext
	private EntityManager em;

	@Override
	public void save(AbstractEntity value) {
		if (value.getId() == null) {
			em.persist(value);
		} else {
			em.merge(value);
		}
	}

	@Override
	public List<Category> findAll() {
		return em.createQuery("from Category", Category.class).getResultList();
	}

	@Override
	public void remove(AbstractEntity value) {
		value = em.find(Category.class, value.getId());
		em.remove(value);
	}

	public Category findCategory(Long id) {
		return em.find(Category.class, id);
	}

	@Override
	public boolean isTableEmpty() {
		Long res=(Long)em.createQuery("select count(1) from Category").getSingleResult();
		return res<=0;
	}
}

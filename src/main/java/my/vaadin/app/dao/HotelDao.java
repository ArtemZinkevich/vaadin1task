package my.vaadin.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import my.vaadin.app.entity.AbstractEntity;
import my.vaadin.app.entity.Category;
import my.vaadin.app.entity.Hotel;

@Repository
public class HotelDao implements IDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void save(AbstractEntity value) {
		if (value.getId() == null) {
			em.persist(value);
		} else {
			em.merge(value);
		}
	}

	@Override
	public List<Hotel> findAll() {
		return em.createQuery("from Hotel", Hotel.class).getResultList();
	}

	@Override
	public void remove(AbstractEntity value) {
		value = em.find(Hotel.class, value.getId());
		em.remove(value);
	}

	public List<Hotel> findAll(String namePattern, String addressPattern) {
		return em.createNamedQuery("Hotel.filter", Hotel.class).setParameter("n", namePattern)
				.setParameter("adr", addressPattern).getResultList();
	}

	@Override
	public boolean isTableEmpty() {
		Long res=(Long)em.createQuery("select count(1) from Hotel").getSingleResult();
		return res<=0;
	}

}

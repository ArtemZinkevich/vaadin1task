package my.vaadin.app.dao;

import java.util.List;

import my.vaadin.app.entity.AbstractEntity;

public interface IDAO {
	void save(AbstractEntity content);
	List<? extends AbstractEntity> findAll();
	void remove (AbstractEntity e);
	boolean isTableEmpty();
}

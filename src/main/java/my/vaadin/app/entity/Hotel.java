package my.vaadin.app.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NamedQueries({
		@NamedQuery(name = "Hotel.filter", query = "SELECT e FROM Hotel AS e WHERE LOWER(e.name) LIKE :n AND LOWER(e.address) LIKE :adr") })
@SuppressWarnings("serial")
@Entity
public class Hotel extends AbstractEntity implements Serializable, Cloneable {
	@NotNull(message = "Name is required")
	@Size(min = 3, max = 40, message = "name must be longer than 3 and less than 40 characters")
	private String name = "";
	@Embedded
	private Payment payment;
	@NotNull(message = "address is required")
	@Size(max = 50, message = "Only 50 characters allowed")
	private String address = "";

	@NotNull(message = "Rating is required")
	private Integer rating;

	private String description;
	@NotNull(message = "OperatesFrom is required")
	@Column(name = "OPERATES_FROM")
	private Long operatesFrom;

	@ManyToOne
	@JoinColumn(name = "CATEGORY_ID")
	private Category category;

	@NotNull(message = "URL is required")
	private String url;

	public boolean isPersisted() {
		return getId() != null;
	}

	@Override
	public String toString() {
		return name + " " + rating + "stars " + address;
	}

	@Override
	public Hotel clone() throws CloneNotSupportedException {
		return (Hotel) super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Hotel() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Long getOperatesFrom() {
		return operatesFrom;
	}

	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Hotel(Long id, String name, String address, Integer rating, Long operatesFrom, Category category,
			String url) {
		super();
		this.setId(id);
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

}
package my.vaadin.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Payment implements Serializable {
	@Column(name="GUARANTY_FEE")
	private Integer guaranteeFee;

	public Integer getGuaranteeFee() {
		return guaranteeFee;
	}

	public void setGuaranteeFee(Integer guaranteeFee) {
		this.guaranteeFee = guaranteeFee;
	}

	public Payment() {
		super();
	}

	public Payment(Payment value) {
		super();
		if(value!=null){
			setGuaranteeFee(value.guaranteeFee);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guaranteeFee == null) ? 0 : guaranteeFee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (guaranteeFee == null) {
			if (other.guaranteeFee != null)
				return false;
		} else if (!guaranteeFee.equals(other.guaranteeFee))
			return false;
		return true;
	}
	

}

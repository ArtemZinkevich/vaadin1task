package my.vaadin.app.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import my.vaadin.app.dao.CategoryDao;
import my.vaadin.app.dao.HotelDao;
import my.vaadin.app.entity.AbstractEntity;
import my.vaadin.app.entity.Category;

@Service
public class CategoryService {
	@Autowired
	private CategoryDao dao;
	private AtomicBoolean isLoaded = new AtomicBoolean(false);

	@Transactional
	public void save(AbstractEntity content) {
		dao.save(content);
	}

	@Transactional
	public void remove(AbstractEntity content) {
		dao.remove(content);
	}

	public Category findCategory(Long id) {
		return dao.findCategory(id);
	}

	@Transactional
	public List<Category> findAll() {
		if(!isLoaded.get()){
			ensureTestData();
			isLoaded.set(true);
		}
		return dao.findAll();
	}

	public CategoryService() {
	}

	@Transactional
	private void ensureTestData() {
		if (dao.isTableEmpty()) {
			save(new Category("Hotel"));
			save(new Category("Hostel"));
			save(new Category("GuestHouse"));
			save(new Category("Appartments"));
		}
	}
}

package test.my.vaadin.app;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAppTest extends AbstractUITest {

	@Test
	public void addCategoryTest() throws InterruptedException {
		// Open localhost:8080;
		driver.get(BASE_URL);
		TimeUnit.SECONDS.sleep(5);
		// Click Categories(Switch to page categories)
		WebElement categoryMenu = driver.findElement(By.xpath(
				"//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(),'Categories')]"));
		categoryMenu.click();
		TimeUnit.SECONDS.sleep(3);
		// find grid size;
		int categoriesSize = driver
				.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class,'v-grid-row')]")).size();
		// click add button
		WebElement addButton = driver.findElement(By.xpath("//div[@role=\"button\"][.//*[contains(text(),\"Add\")]]"));
		addButton.click();
		TimeUnit.SECONDS.sleep(5);
		// Fill in input field
		WebElement categoryNameField = driver.findElement(By.xpath("//input[@type=\"text\"]"));
		categoryNameField.sendKeys("SeleniumCategory");
		TimeUnit.SECONDS.sleep(3);
		// Press save button
		WebElement saveButton = driver
				.findElement(By.xpath("//div[@role=\"button\"][.//*[contains(text(),\"Save\")]]"));
		saveButton.click();
		TimeUnit.SECONDS.sleep(5);
		// Counting grid size
		int categoriesSize2 = driver
				.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class,'v-grid-row')]")).size();
		Assert.assertEquals(1, categoriesSize2 - categoriesSize);
	}

	@Test
	public void addHotelTest() throws InterruptedException {
		// Open localhost:8080;
		driver.get(BASE_URL);
		TimeUnit.SECONDS.sleep(5);
		// click add button
		WebElement addButton = driver.findElement(By.xpath("//div[@role=\"button\"][.//*[contains(text(),\"Add\")]]"));
		addButton.click();
		TimeUnit.SECONDS.sleep(3);
		// get required text elements
		List<WebElement> list = driver.findElements(By.xpath("//input[contains(@class,\"required\")]"));
		list.get(0).sendKeys("SelenuimHotel");
		list.get(1).sendKeys("Selenuim road 66");
		list.get(2).sendKeys("5");
		list.get(3).sendKeys("http://SeleniumHotel.com");
		// select category with "SeleniumCategory"
		WebElement category = driver.findElement(By.xpath("//option[contains(text(),\"SeleniumCategory\")][last()]"));
		category.click();
		TimeUnit.SECONDS.sleep(3);
		// select date
		WebElement date = driver.findElement(By.xpath("//input[@class=\"v-textfield v-datefield-textfield\"]"));
		date.clear();
		date.sendKeys("22.10.10");
		WebElement dateFieldButton = driver.findElement(By.xpath("//button[@class=\"v-datefield-button\"]"));
		dateFieldButton.click();
		TimeUnit.SECONDS.sleep(3);
		// save new hotel
		WebElement saveButton = driver
				.findElement(By.xpath("//div[@role=\"button\"][.//*[contains(text(),\"Save\")]]"));
		saveButton.click();
		TimeUnit.SECONDS.sleep(3);
		//filtering grid to find our SeleniumHotel
		WebElement filterByName = driver
				.findElement(By.xpath("//input[@placeholder=\"filter by name...\"]"));
		filterByName.sendKeys("SelenuimHotel");
		// Counting grid size
		int gridSize = driver
				.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class,'v-grid-row')]")).size();
		Assert.assertTrue(gridSize>0);
		TimeUnit.SECONDS.sleep(3);
	}

}